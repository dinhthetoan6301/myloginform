import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      )
    );
 }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context){
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        child: Column(
          children: [
            emailfield(),
            passwordfield(),
            loginButton(),
          ],
        ),
      ),
    );
  }

  Widget emailfield() {
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email Address'
      ),
    );
  }

  Widget passwordfield() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Password'
      ),
    );
  }

  Widget loginButton() {
    return ElevatedButton(
      onPressed: () {
        print('clicked/Touched');
      },
      child: Text('Login')
    );
  }
}
